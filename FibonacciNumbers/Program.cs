﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FibonacciNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int N, F1 = 0, F2 = 1, rez = 0;
            Console.WriteLine("Введите количество чисел");
            N = Convert.ToInt16(Console.ReadLine());
            Console.WriteLine("Числа Фибоначчи:");
            Console.WriteLine(1);
            for (int i = 1; i < N; i++)
            {
                rez = F1 + F2;
                F1 = F2;
                F2 = rez;
                Console.WriteLine(rez);
            }
            Console.ReadLine();
        }
    }
}
